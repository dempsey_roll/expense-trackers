import React from 'react';

import ChartBar from './ChartBar';

import './Chart.css';

const Chart = props => {
  const dataPointsValue = props.dataPoints.map(dataPoint => dataPoint.value);
  const max = Math.max(...dataPointsValue);
  return (
    <div className="chart">
      {props.dataPoints.map(dataPoint => {
        return <ChartBar 
          value = {dataPoint.value}
          maxValue = {max}
          label = {dataPoint.label} 
          key = {dataPoint.label}
        />;
      })}
    </div>
  );
};

export default Chart;